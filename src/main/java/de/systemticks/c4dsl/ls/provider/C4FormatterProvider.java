package de.systemticks.c4dsl.ls.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.aspectj.runtime.internal.cflowstack.ThreadStackImpl11;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.TextEdit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.systemticks.c4dsl.ls.model.C4CompletionScope;
import de.systemticks.c4dsl.ls.model.C4DocumentModel;
import de.systemticks.c4dsl.ls.utils.C4Utils;
import de.systemticks.c4dsl.ls.utils.LineToken;
import de.systemticks.c4dsl.ls.utils.LineTokenizer;

public class C4FormatterProvider {

    private static final Logger logger = LoggerFactory.getLogger(C4FormatterProvider.class);
    private static final Pattern COMMENT_PATTERN = Pattern.compile("^\\s*?(//|#).*$");

    private final int indentPerScope;
    private LineTokenizer lineTokenizer;

    public C4FormatterProvider(int indentPerScope) {
        this.indentPerScope = indentPerScope;
        this.lineTokenizer = new LineTokenizer();
    }

    public List<TextEdit> calculateFormattedTextEdits(C4DocumentModel model) {

        List<TextEdit> result = new ArrayList<>();
        List<String> rawLines = model.getRawLines();

        for(int lineIdx=0; lineIdx<rawLines.size(); lineIdx++) {
            var currentIdx = lineIdx;
            model.getNearestScope(currentIdx).ifPresent( scope -> {
                String originText = rawLines.get(currentIdx);
                int expectedIndentDepth = getExpectedIndentDepth(scope, currentIdx+1);
                int firstNonWhiteSpace = C4Utils.findFirstNonWhitespace(originText, 0, true);
                if(expectedIndentDepth != firstNonWhiteSpace || hasWhitespacesBetweenTokens(originText)) {
                    String newText = createNewText(originText, expectedIndentDepth);
                    result.add( createTextEdit(newText, originText, currentIdx));
                }    
            });
        }

        return result;
    }
   
    int getExpectedIndentDepth(C4CompletionScope scope, int currentIdx) {
        return (scope.getStartsAt() == currentIdx || scope.getEndsAt() == currentIdx ? scope.getDepth() : scope.getDepth()+1) * indentPerScope;
    }
    
    String createNewText(String oldText, int leadingWhiteSpaces) {
        return (new String(" ")).repeat(leadingWhiteSpaces) + removeWhiteSpacesBetweenTokens(oldText.trim());
    }

    TextEdit createTextEdit(String newText, String oldText, int line) {
        var range = new Range( new Position(line, 0), new Position(line, oldText.length()));
        return new TextEdit(range, newText);
    }

    boolean hasWhitespacesBetweenTokens(String text) {
        List<LineToken> tokens = lineTokenizer.tokenize(text);
        if(tokens.size() > 1) {
            int index = 0;
            while(index < tokens.size()-1) {
                if(tokens.get(index+1).getStart() - tokens.get(index).getEnd() > 1) {
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    String removeWhiteSpacesBetweenTokens(String text) {

        if(isSingleLineComment(text)) {
            return text;
        }

        List<LineToken> tokens = lineTokenizer.tokenize(text);
        if(tokens.size() < 2) {
            return text;
        }

        return tokens.stream().map(LineToken::getToken).collect(Collectors.joining(" "));
    }

    boolean isSingleLineComment(String text) {
        return COMMENT_PATTERN.matcher(text).matches();
    }
}
