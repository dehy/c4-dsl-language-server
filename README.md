# C4 DSL Language Server

This is a language server implementation for the Structurizr DSL.

Structurizr DSL is a textual language that allows developers to describe the architecture of their software system in a structured way, based upon the C4 model. It is part of the Structurizr platform, which provides tools and services for creating, visualizing, and sharing software architecture diagrams.

The C4 DSL Language Server is build upon the Language Server Protocol (LSP), which allows the integration into multiple IDEs and editors, which supports LSP, such as Visual Studio Code, Eclipse and IntelliJ.

## Build

### Clean Build

```
./gradlew clean build
```

### Package and Deploy

```
./gradlew deploy -PcopyTo=<DIR_OF_YOUR_CHOICE>"
```

## C4 DSL Clients

Here is a list of client implementations, that are using the C4 DSL Language Server.

### Visual Studio Code Extension

https://gitlab.com/systemticks/c4-grammar

### Eclipse IDE Plugin (Under construction)

https://gitlab.com/systemticks/c4-dsl-eclipse-plugin

